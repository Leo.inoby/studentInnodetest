const express = require("express");
const student = require("./controller/student");
var bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json());

student(app);

app.listen(3000, function () {});
