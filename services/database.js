const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";
const client = new MongoClient(url);

async function getDb() {
  await client.connect();
  const db = client.db("StudentsManagement");
  return db;
}
async function getCollection(collectionName) {
  const db = await getDb();
  const collection = await db.collection(collectionName);
  return collection;
}
async function getStudents() {
  const students = await getCollection("Students");
  return students;
}

module.exports = {
  getDb,
  getCollection,
  getStudents,
};
