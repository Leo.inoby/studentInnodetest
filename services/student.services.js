const { ObjectId } = require("mongodb");

const { getStudents } = require("./database");

async function listStudents(page = 1, size = 3) {
  const studentsCollection = await getStudents();
  const students = await studentsCollection
    .find()
    .sort({ _id: 1 })
    .skip((parseInt(page) - 1) * parseInt(size))
    .limit(parseInt(size))
    .toArray();
  return students;
}
async function detailStudent(id) {
  const studentsCollection = await getStudents();
  const conditions = { _id: new ObjectId(id) };
  const student = await studentsCollection.find(conditions).toArray();
  return student;
}
async function createStudent(student) {
  const studentsCollection = await getStudents();
  const newStudent = {
    name: student.name,
    className: student.className,
    seatNumber: student.seatNumber,
  };
  await studentsCollection.insertOne(newStudent);
}
async function modifyStudent(student) {
  const studentsCollection = await getStudents();
  const conditions = { _id: new ObjectId(student.id) };
  const update = {
    $set: {
      name: student.name,
      className: student.className,
      seatNumber: student.seatNumber,
    },
  };
  await studentsCollection.updateOne(conditions, update);
}
async function deleteStudent(id) {
  const studentsCollection = await getStudents();
  const conditions = { _id: new ObjectId(id) };

  await studentsCollection.deleteOne(conditions);
}

module.exports = {
  listStudents,
  detailStudent,
  createStudent,
  modifyStudent,
  deleteStudent,
};
