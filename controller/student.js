const {
  listStudents,
  detailStudent,
  createStudent,
  modifyStudent,
  deleteStudent,
} = require("../services/student.services");
const { check, validationResult } = require("express-validator");

function student(app) {
  app.get("/students", async function (req, res) {
    const students = await listStudents(req.query.page, req.query.size);
    return res.send(students);
  });

  app.get(
    "/students/:id",
    [check("id").isAlphanumeric()],
    async function (req, res) {
      if (checkStudent(req)) {
        return res.send("ERROR");
      }
      const student = await detailStudent(req.params.id);
      return res.send(student);
    }
  );

  app.post(
    "/students",
    [
      check("name").isAlpha(),
      check("className").isAlphanumeric(),
      check("seatNumber").isNumeric(),
    ],
    async function (req, res) {
      if (checkStudent(req)) {
        return res.send("ERROR");
      }
      await createStudent(req.body);
      return res.send({ message: "Student created" });
    }
  );

  app.put(
    "/students",
    [
      check("id").isAlphanumeric(),
      check("name").isAlpha(),
      check("className").isAlphanumeric(),
      check("seatNumber").isNumeric(),
    ],
    async function (req, res) {
      if (checkStudent(req)) {
        return res.send("ERROR");
      }
      await modifyStudent(req.body);
      return res.send({ message: "Student updated" });
    }
  );

  app.delete(
    "/students/:id",
    [check("id").isAlphanumeric()],
    async function (req, res) {
      if (checkStudent(req)) {
        return res.send("ERROR");
      }
      await deleteStudent(req.params.id);
      return res.send({ message: "Student delete" });
    }
  );
}

function checkStudent(req) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return true;
  } else {
    return false;
  }
}

module.exports = student;
